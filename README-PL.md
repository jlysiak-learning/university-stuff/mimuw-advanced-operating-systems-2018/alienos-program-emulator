# ZSO 2017/18
## Zadanie 1 - ELFs and Aliens

Autor: Jacek Łysiak

### Opis rozwiązania

Rozwiązanie zadania składa się z dwóch programów:
- `emu` - program zgodny z wymaganiami podanymi w treści zadania
- `loader` - program pomocniczy, uruchamiany jako dziecko `emu` 
        pod kontrolą `ptrace`

Punkt wejścia emulatora znajduje się w pliku `emu.c`.
`emu` na początku działania woła funkcję `fork`. 

Proces potomny skacze do funkcji `run_loader`. Wołany jest 
`ptrace(PTRACE_TRACEME, ...)`, a następnie `execv(LOADER_FILE, argv)`, 
gdzie makro `LOADER_FILE` rozwijane jest do nazwy pliku wykonywalnego 
`loader`-a. Proces ładujący wykonuje sprawdzenie, czy na pewno jest 
śledzony przez inny proces (aby zapobiec przypadkowemu uruchomieniu 
programu z bardzo niebezpieczym kodem obcych). Następnie czytany jest 
nagłówek podanego pliku ELF oraz jego segmenty. Wykonywane są podstawowe
sprawdzenia poprawności danych (typy segmentów i adresy). `loader` ładuje 
do pamięci dane z pliku i tuż przed wykonaniem skoku do kodu obcych woła 
funkcję `raise(SIGSTOP)`, aby poinformować rodzica, że zakończył już 
proces ładowania i chce przejść do trybu emulacji. Proces `emu` wznawia 
`loader`-a przez wywołanie `ptrace(PTRACE_SYSEMU, ...)`. Po wznowieniu 
program wykonuje kod obcych zatrzymując się na każdym syscall-u.

Proces-rodzic po wykonaniu `fork`-a przechodzi do `run_emu`,
gdzie czeka na wykonanie `execv`(`SIGTRAP`) oraz ukończneie ładowania 
pliku ELF do pamięci(`SIGSTOP`) przez `loader`.
W tym momencie zmieniane są rejestry RIP i RSP procesu dziecka.
RIP wskazuje na entry point obcej binarki, a RSP jest ustawione
na specjalnie zaalokowany obszar pamięci wielkości 4MB.
Wznawiamy dziecko z flagą `PTRACE_SYSEMU`.
Natępnie, w pętli głównej emulator czeka na sygnał. Jeśli proces potomny 
zatrzymał się na syscall-u, kopiowana jest zawartość jego rejestrów. 
Dalej następuje sprawdzenie poprawności wołanego syscall-a 
i próba jego emulacji. W przypadku wykrycia nieprawidłowości, 
poszczególne funkcje emulujące syscall-e zwracają błąd. Proces 
potomny jest zabijany, a emulator kończy pracę ze stosownym kodem błędu.
W przypadku, gdy wszystko jest poprawne, wynik syscall-a 
przekazywany jest do rejestru `rax` procesu potomnego 
i wznawiane jest jego wykonanie wołaniem `ptrace(PTRACE_SYSEMU, ...)`.

### Kompilacja i uruchomienie

Wymagania:
- ncurses (jest na obraze QEMU)

Kompilacja:
- `make` w katalogu głównym

Uruchomienie:
- `./emu <alien prog> [params]` z katalogu głównego



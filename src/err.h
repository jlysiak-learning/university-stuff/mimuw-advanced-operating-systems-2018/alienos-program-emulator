/*
 * Jacek Lysiak
 *
 * Errors + messages
 *
 */

#ifndef _ERR_H_
#define _ERR_H_

#include <errno.h>
#include <string.h>
#include <stdio.h>

/* Common return codes. */
#define RET_ERR                 (-1)
#define RET_OK                  0

/* Status codes and masks */
#define EXIT_REQ_MASK           0x80
#define FAULT_MASK              0x40
#define ALIEN_ECODE_MASK        0x3f    // 0-63

#define INVALID_ECODE(code)    (((code) & ~ALIEN_ECODE_MASK) != 0)

#define FAULT_ECODE             0x7f    // 127


/* Emulator control codes. */
#define OK_CONT                 0
#define OK_EXIT(aliencode)      (((aliencode) & ALIEN_ECODE_MASK) | EXIT_REQ_MASK)
#define FAULT_EXIT              (FAULT_ECODE | EXIT_REQ_MASK)

#define SHOULD_EXIT(status)     (((status) & EXIT_REQ_MASK) == EXIT_REQ_MASK)
#define ECODE(status)           \
    ((status) & FAULT_MASK ? FAULT_ECODE : (status) & (ALIEN_ECODE_MASK))


/* Error message buffer. */
#define ERRMSGSZ    512
extern char errmsg[ERRMSGSZ];

#define SHOW_ERR_MSG() \
    fprintf(stderr, "%s\n", errmsg)

#define STR(X)      #X
#define STRSTR(X)   STR(X)
#define STRLINE     STRSTR(__LINE__)

#define SYSERR(cond, msg)                                                   \
    do {                                                                    \
        if ((cond)) {                                                       \
            fprintf(stderr, "*** SYSTEM ERROR @ "                           \
                    __FILE__":" STRLINE "\nCondition: "                     \
                    #cond "\r\nMessage: " msg "\r\n");                      \
            perror("Error msg");                                            \
            exit(EXIT_FAILURE);                                             \
        }                                                                   \
    } while(0)

#define EMUERR(cond, msg)                                                   \
    do {                                                                    \
        if ((cond)) {                                                       \
            fprintf(stderr, "*** EMU ERROR @ "                              \
                    __FILE__":" STRLINE "\nCondition: "                     \
                    #cond "\r\nMessage: " msg "\r\n");                      \
            exit(EXIT_FAILURE);                                             \
        }                                                                   \
    } while(0)

#define FAILMSG(msg)                                                        \
    sprintf(errmsg, "*** ALIEN PROGRAM FAIL \nMessage: %s\n", msg)

#define RETERR(cond)                                                        \
    do {                                                                    \
        if ((cond))                                                         \
            return RET_ERR;                                                 \
    } while(0)

#define RETERRMSG(cond, msg)                                                \
    do {                                                                    \
        if ((cond)) {                                                       \
            sprintf(errmsg, "*** EMULATOR ERROR @ " __FILE__                \
                    ":" STRLINE "\nCondition: " #cond                       \
                    "\nMessage: %s\n", msg);                                \
            return RET_ERR;                                                 \
        }                                                                   \
    }while(0)

#define ERRMSG(cond, msg)                                                   \
    do {                                                                    \
        if ((cond)) {                                                       \
            sprintf(errmsg, "*** EMULATOR ERROR @ " __FILE__                \
                    ":" STRLINE "\nCondition: " #cond                       \
                    "\nMessage: %s\n", msg);                                \
        }                                                                   \
    }while(0)

#endif // _ERR_H_

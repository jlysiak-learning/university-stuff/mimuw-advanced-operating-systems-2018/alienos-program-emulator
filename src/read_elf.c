/*
 * Jacek Lysiak
 *
 * ELF files handling
 *
 */

//#define _BSD_SOURCE
#define _POSIX_C_SOURCE 200809L

#include <unistd.h>
#include <stdlib.h>

#include "read_elf.h"
#include "err.h"

ssize_t read_bytes(int fd, void *buff, size_t len, off_t off)
{
    ssize_t rsize;
    rsize = pread(fd, buff, len, off);
    if (rsize < 0)
        return RET_ERR;
    return rsize;
}

int read_elf_hdr(int fd, Elf64_Ehdr *hdr)
{
    return read_bytes(fd, hdr, sizeof(Elf64_Ehdr), 0); 
}

int read_elf_prog_hdr(int fd, unsigned long off, Elf64_Phdr *phdr)
{
    return read_bytes(fd, phdr, sizeof(Elf64_Phdr), off); 
}

int read_elf_sect_hdr(int fd, unsigned long off, Elf64_Shdr *shdr)
{
    return read_bytes(fd, shdr, sizeof(Elf64_Shdr), off);
}

void iter_elf_prog_hdr(int fd, void (*phdr_cb)(Elf64_Phdr *phdr, void *aux), void *aux)
{
    unsigned long off; 
    Elf64_Ehdr hdr;
    Elf64_Phdr phdr;

    read_elf_hdr(fd, &hdr);
    off = hdr.e_phoff;
    for (int i = 0; i < hdr.e_phnum; ++i) {
        read_elf_prog_hdr(fd, off, &phdr);
        (*phdr_cb)(&phdr, aux);
        off += hdr.e_phentsize;
    }
}

void iter_elf_sect_hdr(int fd, void (*shdr_cb)(Elf64_Shdr *shdr, void *aux), void *aux)
{
    unsigned long off; 
    Elf64_Ehdr hdr;
    Elf64_Shdr shdr;

    read_elf_hdr(fd, &hdr);
    off = hdr.e_shoff;
    for (int i = 0; i < hdr.e_shnum; ++i) {
        read_elf_sect_hdr(fd, off, &shdr);
        (*shdr_cb)(&shdr, aux);
        off += hdr.e_shentsize;
    }
}


void print_elf_hdr_info(Elf64_Ehdr *hdr)
{
    printf("=== ELF64 HEADER ====\n");
    printf("MAGIC:");
    for (int i = 0; i < EI_NIDENT; ++i)
        printf("0x%02x ", hdr->e_ident[i]);
    printf("\nTYPE: %X\n", hdr->e_type);
    printf("MACHINE: %X\n", hdr->e_machine);
    printf("VERSION: %X\n", hdr->e_version);
    printf("ENTRY: %lX\n", hdr->e_entry);
    printf("PROG HEADER TAB OFFSET: %lX\n", hdr->e_phoff);
    printf("SECT HEADER TAB OFFSET: %lX\n", hdr->e_shoff);
    printf("FLAGS: %X\n", hdr->e_flags);
    printf("ELF HEADER SIZE: %X\n", hdr->e_ehsize);
    printf("PROG HEADER ENTRY SIZE: %X\n", hdr->e_phentsize);
    printf("PROG HEADER NUM: %X\n", hdr->e_phnum);
    printf("SECT HEADER ENTRY SIZE: %X\n", hdr->e_shentsize);
    printf("SECT HEADER NUM: %X\n", hdr->e_shnum);
    printf("SECT HEADER STRNDX: %X\n\n", hdr->e_shstrndx);
}

void print_elf_phdr_info(Elf64_Phdr *phdr)
{
    printf("=== ELF64 PROGRAM HEADER ====\n");
    printf("TYPE: %X\n", phdr->p_type);
    printf("FLAGS: %X\n", phdr->p_flags);
    printf("SEGMENT FILE OFFSET: %lX\n", phdr->p_offset);
    printf("SEGMENT VIRT ADDRESS: %lX\n", phdr->p_vaddr);
    printf("SEGMENT PHYS ADDRESS: %lX\n", phdr->p_paddr);
    printf("SEGMENT SIZE IN FILE: %lX\n", phdr->p_filesz);
    printf("SEGMENT SIZE IN MEMORY: %lX\n", phdr->p_memsz);
    printf("SEGMENT ALIGNMENT: %lX\n\n", phdr->p_align);
}

void print_elf_shdr_info(Elf64_Shdr *shdr)
{
    printf("=== ELF64 SECTION HEADER ====\n");
    printf("SECTION NAME: %X\n", shdr->sh_name);
    printf("SECTION TYPE: %X\n", shdr->sh_type);
    printf("SECTION FLAGS: %lX\n", shdr->sh_flags);
    printf("ADDRES: %lX\n", shdr->sh_addr);
    printf("FILE OFFSET: %lX\n", shdr->sh_offset);
    printf("SECTION SIZE: %lX\n", shdr->sh_size);
    printf("LINK: %X\n", shdr->sh_link);
    printf("INFO: %X\n", shdr->sh_info);
    printf("ADDRESS ALIGNMENT: %lX\n", shdr->sh_addralign);
    printf("ENTRY SIZE: %lX\n", shdr->sh_entsize);
}

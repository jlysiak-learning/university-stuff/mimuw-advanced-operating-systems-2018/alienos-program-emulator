/*
 * Jacek Lysiak
 *
 * Memory utilities
 *
 */

#include <unistd.h>

void *round_down(void *ptr)
{
    long pagesz = sysconf(_SC_PAGESIZE);
    return (void *)((unsigned long) ptr / pagesz * pagesz);
}

void *round_up(void *ptr)
{
    long pagesz = sysconf(_SC_PAGESIZE);
    return (void *)(((unsigned long) ptr / pagesz + 1) * pagesz);
}

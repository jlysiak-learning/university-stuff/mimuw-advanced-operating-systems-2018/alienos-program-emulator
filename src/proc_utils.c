/*
 * Jacek Lysiak
 *
 * Process helpers methods
 *
 */

#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <string.h>

#include "err.h"

#define LSIZE   (sizeof(long))

/* Handy union to copy data using ptrace. */
typedef union dataholder {
    long val;
    char arr[LSIZE];
} dhold;

int wait_proc(pid_t pid, int sig)
{
    int status;
    RETERRMSG(waitpid(pid, &status, 0) < 0, "waitpid failed");
    RETERRMSG(WIFEXITED(status), "process exited");
    RETERRMSG(!WIFSTOPPED(status), "process not stopped");
    RETERRMSG(WSTOPSIG(status) != sig, "unexpected stop signal");
    return RET_OK;
}

int cpy_from_proc(pid_t pid, char *dest, char *src, int sz)
{
    dhold data;
    int left = sz;
    int lsz = LSIZE;
    while (left > 0) {
        errno = 0;
        data.val = ptrace(PTRACE_PEEKDATA, pid, src, NULL);
        RETERRMSG(errno < 0, "ptrace peek data");
        memcpy(dest, data.arr, left > lsz ? lsz : left);
        dest += LSIZE;
        src += LSIZE;
        left = left - LSIZE;
    }
    return RET_OK;
}

/*
 * Jacek Lysiak
 *
 * Process utilities
 *
 */

#ifndef _PROCESS_UTILS_H_
#define _PROCESS_UTILS_H_

/* Wait for child's signal.
 * Error if recv signal !=sig. */
int wait_proc(pid_t pid, int sig);

/* Copy bytes from process to buffer. */
int cpy_from_proc(pid_t pid, char *dest, char *src, int sz);

#endif // _PROCESS_UTILS_H_

/*
 * Jacek Lysiak
 *
 * AlienOS syscalls
 *
 */

#ifndef _ALIENOS_SYSCALL_H_
#define _ALIENOS_SYSCALL_H_

#define ALIEN_SYS_end           0
#define ALIEN_SYS_getrand       1
#define ALIEN_SYS_getkey        2
#define ALIEN_SYS_print         3
#define ALIEN_SYS_setcursor     4

#endif  // _ALIENOS_SYSCALL_H_

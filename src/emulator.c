/*
 * Jacek Lysiak
 *
 * AlienOS program emulator routies
 *
 */

//#define _POSIX_SOURCE
#define _GNU_SOURCE

#include <sys/user.h>       // user_regs_struct
#include <sys/ptrace.h>
#include <linux/ptrace.h>   // req. for: PTRACE_SYSEMU 
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <stdint.h>

#include "screen.h"
#include "alienos_syscall.h"
#include "alienos_term.h"
#include "alienos_elf.h"
#include "proc_utils.h"
#include "err.h"

static int fail(char *info)
{
    FAILMSG(info);
    return FAULT_EXIT;
}

// void noreturn end(int status)
static int emulate_end(struct user_regs_struct *ctx)
{
    uint64_t ecode = ctx->rdi;
    if (INVALID_ECODE(ecode))
        return fail("Invalid exit code");
    return OK_EXIT(ecode);
}

#define MY_SYS_getrandom 318 
// uint32_t getrand()
static int emulate_getrand(struct user_regs_struct *ctx)
{
    uint32_t v;
    long r = syscall(MY_SYS_getrandom, (void *) &v, sizeof(uint32_t), 0);
    RETERRMSG(r < 0, "get random problem");
    ctx->rax = v;
    return OK_CONT;
}

// int getkey()
static int emulate_getkey(struct user_regs_struct *ctx)
{
    int key = ascr_get_key();
    ctx->rax = (uint64_t) key;
    return OK_CONT;
}

// void print(int x, int y, uint16_t *chars, int n)
static int emulate_print(pid_t pid, struct user_regs_struct *ctx)
{
    char *str;
    char *col;
    uint16_t *chars;
    int n = (int) ctx->r10;
    int x = (int) ctx->rdi;
    int y = (int) ctx->rsi;
    uint16_t *proc_chars = (uint16_t *) ctx->rdx;
    if (INVALID_POINTER(proc_chars))
        return fail("Invalid pointer");
    if (INVALID_XYTEXT(x, y, n))
        return fail("Invalid text position and length");
    RETERRMSG((str = malloc(2 * n * sizeof(char))) == NULL, "str malloc failed");
    col = str + n;
    chars = malloc(n * sizeof(uint16_t));
    if (chars == NULL) {
        free(str);
        RETERRMSG(1, "chars malloc failed");
    }
    RETERR(cpy_from_proc(pid, (char *) chars, (char *) proc_chars, n * sizeof(uint16_t)) < 0);
    for (int i = 0; i < n; ++i) {
        if (INVALID_CHAR(chars[i]))
            return fail("Invalid character"); 
        str[i] = GET_CHAR(chars[i]);
        col[i] = GET_COLOR(chars[i]);
    }
    ascr_print(x, y, str, col, n);
    free(str);
    free(chars);
    return OK_CONT;
}

// AlienOS syscall: void setcursor(int x, int y)
static int emulate_setcursor(struct user_regs_struct *ctx)
{
    int x = (int) ctx->rdi;
    int y = (int) ctx->rsi;
    if (INCORRECT_XY(x, y))
        return fail("Invalid cursor position");
    ascr_setcursor(x, y);
    return OK_CONT;
}

static int emulate_syscall(pid_t pid, struct user_regs_struct *ctx)
{
    switch (ctx->orig_rax) {
        case ALIEN_SYS_end:
            return emulate_end(ctx);
        case ALIEN_SYS_getrand:
            return emulate_getrand(ctx);
        case ALIEN_SYS_getkey:
            return emulate_getkey(ctx);
        case ALIEN_SYS_print:
            return emulate_print(pid, ctx);
        case ALIEN_SYS_setcursor:
            return emulate_setcursor(ctx);
        default:
            return fail("Invalid syscall nr");
    }
}

int emu_main(pid_t pid)
{
    int estatus;
    struct user_regs_struct regs;

    while (1) {
        RETERR(wait_proc(pid, SIGTRAP) < 0);
        RETERRMSG(ptrace(PTRACE_GETREGS, pid, NULL, &regs) < 0, "ptrace get regs");
        RETERR((estatus = emulate_syscall(pid, &regs)) < 0);
        if(SHOULD_EXIT(estatus))
            break;
        RETERRMSG(ptrace(PTRACE_SETREGS, pid, NULL, &regs) < 0, "ptrace set regs");
        RETERRMSG(ptrace(PTRACE_SYSEMU, pid, NULL, NULL) < 0, "ptrace sysemu");
    }
    return ECODE(estatus);
}


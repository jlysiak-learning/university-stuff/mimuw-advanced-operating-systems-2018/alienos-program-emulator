/*
 * Jacek Lysiak
 *
 * AlienOS screen support
 */

#ifndef _SCREEN_H_
#define _SCREEN_H_

void ascr_setcursor(int x, int y);

void ascr_print(int x, int y, char *str, char *col, int n);

void ascr_init();

void ascr_end();

int ascr_get_key();

#endif // _SCREEN_H_

/*
 * Jacek Lysiak
 *
 * ELF files handling header
 *
 */

#ifndef _ELF_READ_H_
#define _ELF_READ_H_

#include <elf.h>

ssize_t read_bytes(int fd, void *buff, size_t len, off_t off);

/* Read ELF header from file into ELF64 header struct. */
int read_elf_hdr(int fd, Elf64_Ehdr *hdr);

/* Read ELF program header from file into phder struct. */
int read_elf_prog_hdr(int fd, unsigned long off, Elf64_Phdr *phdr);

/* Read ELF section header from file into phder struct. */
int read_elf_sect_hdr(int fd, unsigned long off, Elf64_Shdr *shdr);

/* Iterate over all program headers and run callbacks. */
void iter_elf_prog_hdr(int fd, void (*phdr_cb)(Elf64_Phdr *phdr, void *aux), void *aux);

/* Iterate over all section headers and run callbacks. */
void iter_elf_sect_hdr(int fd, void (*shdr_cb)(Elf64_Shdr *shdr, void *aux), void *aux);

/* Show ELF64 header content. */
void print_elf_hdr_info(Elf64_Ehdr *hdr);

/* Show ELF64 program header content. */
void print_elf_phdr_info(Elf64_Phdr *phdr);

/* Show ELF64 section header content. */
void print_elf_shdr_info(Elf64_Shdr *shdr);

#endif // _ELF_READ_H_

/*
 * Jacek Lysiak
 *
 * AlienOS ELF params & utils
 *
 */

#ifndef _ALIEN_ELF_H_
#define _ALIEN_ELF_H_

/* PT_PARAMS id */
#define PT_PARAMS           0x60031337

/* Alien program stack params */
#define STACKSZ         (1024 * 4096)
#define STACK_ADDR      VA_SPACE_END
#define STACK_PROT      (PROT_WRITE | PROT_READ)
#define STACK_FLAGS     (MAP_ANON | MAP_PRIVATE | MAP_FIXED)

/* Virtual address space limits */
#define VA_SPACE_BEGIN      0x31337000UL
#define VA_SPACE_END        0x80000000UL
#define VA_SZ               (VA_SPACE_END - VA_SPACE_BEGIN)
#define INVALID_POINTER(p) \
    ((p) == 0 || (unsigned long)(p) < VA_SPACE_BEGIN \
     || (VA_SPACE_END + STACKSZ) < (unsigned long)(p))

/* ELF header tests */
#define TYPE_NOT_EXEC(ehdr) \
    (ehdr->e_type != ET_EXEC)
#define MACHINE_NOT_X8664(ehdr) \
    (ehdr->e_machine != EM_X86_64)
#define INVALID_ENTRY(ehdr) \
    INVALID_POINTER(ehdr->e_entry)

/* ELF segments simple checks */
#define INVALID_SEG_VASPACE(addr, memsz) \
    (INVALID_POINTER(addr) || INVALID_POINTER((addr) + (memsz)))
#define INVALID_SEG_TYPE(type)  \
    ((type) != PT_LOAD && (type) != PT_PARAMS)

/* Default flags for mmap */
#define PROT_FLAGS          (PROT_READ | PROT_WRITE)
/* PT_PARAM argument size */
#define ARGSZ               4
/* Section prot flags bitmast */
#define PHDR_PROT_MASK      0x7

#endif // _ALIEN_ELF_H_

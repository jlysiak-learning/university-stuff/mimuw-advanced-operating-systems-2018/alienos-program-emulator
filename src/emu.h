/*
 * Jacek Lysiak
 *
 * emu stuff
 *
 */

#ifndef _EMU_H_
#define _EMU_H_


#define USAGE                               \
    "Usage:\n"                              \
    "\t%s <AlienOS prog> [params ...]\n"

#define LOADER_FILE "./loader"

#endif // _EMU_H_

/* 
 * Jacek Lysiak
 *
 * Screen printing etc.
 *
 */

#include <ncurses.h>
#include "alienos_term.h"

void ascr_init()
{
    initscr();
    keypad(stdscr, TRUE);
    noecho();

    start_color();
    init_pair(BLACK, COLOR_BLACK, COLOR_BLACK);
    init_pair(BLUE, COLOR_BLUE, COLOR_BLACK);
    init_pair(GREEN, COLOR_GREEN, COLOR_BLACK);
    init_pair(TURQUOISE, COLOR_CYAN, COLOR_BLACK);
    init_pair(RED, COLOR_RED, COLOR_BLACK);
    init_pair(PINK, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(YELLOW, COLOR_YELLOW, COLOR_BLACK);
    init_pair(LGREY, COLOR_BLACK, COLOR_WHITE);
    init_pair(LGREEN, COLOR_GREEN, COLOR_WHITE);
    init_pair(LTURQUOISE, COLOR_CYAN, COLOR_WHITE);
    init_pair(LRED, COLOR_RED, COLOR_WHITE);
    init_pair(LPINK, COLOR_MAGENTA, COLOR_WHITE);
    init_pair(LYELLOW, COLOR_YELLOW, COLOR_WHITE);
    init_pair(WHITE, COLOR_WHITE, COLOR_BLACK);

    refresh();
}

void ascr_end()
{
    echo();
    endwin();
}

void ascr_setcursor(int x, int y)
{
    move(y, x);
}

/* Print n-char string starting from (x,y) and using colors from col.
 * DO NOT change cursor position on the screen! */
void ascr_print(int x, int y, char *str, char *col, int n)
{
    n = n;
    int ox, oy;
    getyx(stdscr, oy, ox);
    move(y, x);
    for (int i = 0; i < n; ++i) {
        attron(COLOR_PAIR(col[i]));
        printw("%c", str[i]);
        attroff(COLOR_PAIR(col[i]));
    }
    move(oy, ox);
    refresh();
}

int ascr_get_key()
{
    while (1) {
        int key = getch();
        switch (key) {
            case KEY_UP:
                return ALIEN_KEY_UP; 
            case KEY_DOWN:
                return ALIEN_KEY_DOWN; 
            case KEY_LEFT:
                return ALIEN_KEY_LEFT; 
            case KEY_RIGHT:
                return ALIEN_KEY_RIGHT; 
            case ALIEN_KEY_ENTER:
                return ALIEN_KEY_ENTER;
            default:
                if (!INVALID_CHAR(key))
                    return key;
        }
    }
}

/*
 * Jacek Lysiak
 *
 * AlienOS program loader
 * NOTICE:
 *  All check-overheads are due to its alieness...
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include "err.h"
#include "memory.h"
#include "alienos_elf.h"
#include "read_elf.h"
#include "loader.h"

/* ELF program header iterator callback.
 * `iter_elf_prog_hdr` in `read_elf.c`
 * reads all program headers and
 * calls this function on each of them. 
 * This one, first, loads all PT_LOAD segments. */ 
static void mmap_phdrs_iter_cb(Elf64_Phdr *phdr, void *args)
{
    if (PHDR_ERR(args))
        goto phdr_err;
    if (INVALID_SEG_VASPACE(phdr->p_vaddr, phdr->p_memsz)) 
        goto phdr_err;
    if (INVALID_SEG_TYPE(phdr->p_type)) 
        goto phdr_err;
    void *begin = round_down((void *) phdr->p_vaddr);
    void *end = (void *) (phdr->p_vaddr + phdr->p_memsz);
    size_t len = MEMSZ(end, begin);
    if (phdr->p_type == PT_LOAD) {
        void * r = mmap(begin, len, PROT_FLAGS, MAP_PRIVATE | MAP_ANON, -1, 0);
        if ((long) r < 0) 
            goto phdr_err;
        read_bytes(FD(args), (void *) phdr->p_vaddr, phdr->p_filesz, phdr->p_offset);
        // mmap guarantees that MAP_ANON returns zero-filled pages
        // so don't care about memory left
    } else { 
        // PT_PARAMS section
        // copy for later use
        memcpy(&PTPARAM(args), phdr, sizeof(Elf64_Phdr));
    }
    return;
phdr_err:
    SET_PHDR_ERR(args);
}

static int set_pt_params(int argc, char **argv, Elf64_Phdr *ptpar)
{
    int al_argc = ptpar->p_memsz / ARGSZ;
    RETERRMSG(al_argc > argc, "Not enougth paraeters!");
    char *arg_ptr = (char *) ptpar->p_vaddr;
    u32h data;
    for (int i = 0; i < al_argc; ++i) {
        data.v = atoi(argv[i]);
        memcpy(arg_ptr, data.arr, ARGSZ);
        arg_ptr += ARGSZ;
    }
    return RET_OK;
}

static void mprot_iter_cb(Elf64_Phdr *phdr, void *args)
{
    if (PHDR_ERR(args))
        return;
    void *begin = round_down((void *) phdr->p_vaddr);
    void *end = (void *) (phdr->p_vaddr + phdr->p_memsz);
    size_t len = MEMSZ(end, begin);
    int prot = phdr->p_flags & PHDR_PROT_MASK;
    if (mprotect(begin, len, prot) < 0) 
        SET_PHDR_ERR(args);
}

int invalid_elf_hdr(Elf64_Ehdr *ehdr)
{
    if (TYPE_NOT_EXEC(ehdr))
        return RET_ERR;
    if (MACHINE_NOT_X8664(ehdr))
        return RET_ERR;
    if (INVALID_ENTRY(ehdr))
        return RET_ERR;
    return RET_OK;
}


int main(int argc, char **argv)
{
    EMUERR(argc < 2, "Path to AlienOS binary file required!");
    EMUERR(ptrace(PTRACE_TRACEME, 0, NULL, NULL) != -1, 
            "Loader not traced by emulator!");
    
    // Block terminal size change signals
    sigset_t set;
    SYSERR(sigemptyset(&set) < 0, "sigemptyset");
    SYSERR(sigaddset(&set, SIGWINCH) < 0, "sigaddset");
    SYSERR(sigprocmask(SIG_BLOCK, &set, NULL) < 0, "Cannot block SIGWINCH");

    // No UNIX process uses va space in range [VA_SPACE_BEGIN : VA_SPACE_END]
    // I'm not munmmap-ing it
    
    int fd = open(argv[1], O_RDONLY);
    SYSERR(fd < 0, "opening ELF file");

    Elf64_Ehdr ehdr;
    if (read_elf_hdr(fd, &ehdr) < 0)
        goto fail;
    if (invalid_elf_hdr(&ehdr))
        goto fail;
    
    struct iter_args args;
    args.err = 0;
    args.fd = fd;
    
    // Run over Phdrs, do validation, mmap and fill with data
    iter_elf_prog_hdr(fd, mmap_phdrs_iter_cb, (void *) &args);
    if (args.err)
        goto fail;
    if (set_pt_params(argc - 2, argv + 2, &args.ptparams) < 0)
        goto fail;
    // Set protection flags
    iter_elf_prog_hdr(fd, mprot_iter_cb, (void *) &args);
    if (args.err)
        goto fail;
    SYSERR(close(fd) < 0, "closing ELF file descriptor");
    
    // Allocate new small fixed stack 
    long r = (long) mmap((void *) STACK_ADDR, STACKSZ, STACK_PROT, STACK_FLAGS, -1, 0);
    SYSERR(r < 0, "stack mmap failed!");

    SYSERR(raise(SIGSTOP) < 0, "raise");
    // After raising SIGSTOP traces changes RIP and RSP of tracee
    // We shouldn't back here.

fail:
    close(fd);
    SHOW_ERR_MSG();
    return EXIT_FAILURE;
}


/*
 * Jacek Lysiak
 *
 * AlienOS terminal macros & defs
 */

#ifndef _ALIEN_TERM_H_
#define _ALIEN_TERM_H_

/* Colors */
#define BLACK           0
#define BLUE            1
#define GREEN           2
#define TURQUOISE       3
#define RED             4
#define PINK            5
#define YELLOW          6
#define LGREY           7
#define DGREY           8
#define LBLUE           9
#define LGREEN          10
#define LTURQUOISE      11
#define LRED            12
#define LPINK           13
#define LYELLOW         14
#define WHITE           15

/* Terminal config */
#define TERM_W          80
#define TERM_H          24
#define INCORRECT_XY(x, y)  \
    ((x) < 0 || TERM_W < (x) || (y) < 0 || TERM_H < (y))
#define INVALID_XYTEXT(x, y, n) \
    ((x) < 0 || (TERM_W - (n)) < (x) || (y) < 0 || TERM_H < (y))

/* Characters macros */
#define MASK_CHAR       0x00ff
#define GET_CHAR(x)     ((x) & MASK_CHAR)
#define MASK_COLOR      0x0f00
#define GET_COLOR(x)    (((x) & MASK_COLOR) >> 8)
#define MASK_USED       0x0fff
#define MASK_UNUSED     0xf000

#define CHAR_LIM_D      0x20
#define CHAR_LIM_U      0x7e
#define INVALID_CHAR(c)    \
    (GET_CHAR(c) < CHAR_LIM_D || CHAR_LIM_U < GET_CHAR(c))

/* Key mapping */
#define ALIEN_KEY_UP          0x80
#define ALIEN_KEY_LEFT        0x81
#define ALIEN_KEY_DOWN        0x82
#define ALIEN_KEY_RIGHT       0x83
#define ALIEN_KEY_ENTER       0x0a

#endif // _ALIEN_TERM_H_

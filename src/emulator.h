/*
 * Jacek Lysiak
 *
 * AlienOS program emulator routies
 *
 */

#ifndef _EMULATOR_H_
#define _EMULATOR_H_

/* Starts emulator main syscall loop.
 * Assumes that tracee with given `pid` is traced
 * with `PTRACE_SYSEMU` flag set. 
 * Returns correct exit code. 
 * Responsible for killing process before return. */
int emu_main(pid_t pid);

#endif // _EMULATOR_H_

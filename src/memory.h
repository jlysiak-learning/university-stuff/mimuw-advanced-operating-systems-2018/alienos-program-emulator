/*
 * Jacek Lysiak
 *
 * Memory utilities
 *
 */

#ifndef _MEMORY_UTILS_H_
#define _MEMORY_UTILS_H_

/* Calculate memory size between two given pointers.*/
#define MEMSZ(p1, p2) \
    ((long long) p1 - (long long) p2)

/* Round down address to system page size. */
void *round_down(void *ptr);

/* Round up address to system page size. */
void *round_up(void *ptr);

#endif  // _MEMORY_UTILS_H_

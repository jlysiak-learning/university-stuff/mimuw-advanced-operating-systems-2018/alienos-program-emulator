#ifndef _LOADER_H_
#define _LOADER_H_

#include <elf.h>

/* Handy holder for PT_PARAMS arg */
typedef union u32holder {
    uint32_t v;
    char arr[ARGSZ];
} u32h;

/* phdr interator set of arguments... */
struct iter_args {
    int err;
    int fd;
    Elf64_Phdr ptparams;
};

/* ... and some macros */
#define PHDR_ERR(args)          (((struct iter_args *)(args))->err)
#define SET_PHDR_ERR(args)      (((struct iter_args *)(args))->err = 1)
#define FD(args)                (((struct iter_args *)(args))->fd)
#define PTPARAM(args)           (((struct iter_args *)(args))->ptparams)

#endif // _LOADER_H_

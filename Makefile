TARGETS=loader emu

SRC_DIR=src
BUILD_DIR=build

FILES = $(notdir $(basename $(wildcard $(SRC_DIR)/*.c)))
SHARED = $(filter-out $(TARGETS),$(FILES))
SRCS = $(SHARED:%=%.c)
OBJS = $(SRCS:%.c=%.o)

CC=gcc

#DBG=-DDEBUG
DBG=
CFLAGS = -std=gnu11 -g -Wall -Wextra -Werror -pedantic $(DBG)
LDFLAGS= -lcurses

#=========================================================================================

all: $(TARGETS)


$(TARGETS):% : $(addprefix $(BUILD_DIR)/,$(OBJS) %.o)
	$(CC) $^ $(LDFLAGS) -o $(BUILD_DIR)/$@
	cp $(BUILD_DIR)/$@ $@


$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c | $(BUILD_DIR)
	$(CC) $(CFLAGS) -c $< -o $@


$(BUILD_DIR):
	mkdir -p $@


.PHONY: clean todo

clean:
	rm -rf $(BUILD_DIR) $(TARGETS)

todo:
	@grep -rniE --color=auto "TODO" src/*
